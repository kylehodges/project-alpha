from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import CreateTask
from tasks.models import Task


# Create your views here.
@login_required
def create_task(request):
    if request.method == "GET":
        form = CreateTask()

    else:
        form = CreateTask(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    context = {"form": form}
    return render(request, "tasks/create_task.html", context)


@login_required
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)

    context = {"tasks": tasks}
    return render(request, "tasks/mine.html", context)
